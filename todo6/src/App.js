import './App.css';
import { useState, useEffect } from 'react'
function App() {
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const [storeVal, setStoreVal] = useState("")
  const [isEdit, setEdit] = useState(false)
  // const [currentRegister,setCurrentRegister]=useState({})
  const [currentEmail, setCurrentEmail] = useState({})
  const [currentPass, setCurrentPass] = useState({})
  const [arrRegister, setArrRegister] = useState(() => {
    const saveArrRegister = localStorage.getItem("arrRegister")
    if (saveArrRegister) {
      return JSON.parse(saveArrRegister)
    } else {
      return []
    }
  })
  const handleAdd = e => {
    e.preventDefault();

    if (storeVal !== 0) {
      setArrRegister([
        ...arrRegister, {

          "id": Math.floor(Math.random() * 1000),
          "email": email.trim(),
          "password": password.trim()
        }
      ])
    }

    setStoreVal('')
  }
  const deleteClick = id => {
    const remove = [...arrRegister].filter(email => email.id !== id)
    setArrRegister(remove)
  }
  const handleEditClick=()=>{
    setEdit(true)
    setCurrentEmail({...email})
    setCurrentPass({...password})  
  }
// handlefrmUpdate=()=>{
//   setCurrentEmail(currentEmail.id,currentEmail)
// }
  useEffect(() => {
    localStorage.setItem("arrRegister", JSON.stringify(arrRegister))
  }, [arrRegister])




  return (
    <div className="App">
      {
        isEdit ? <div class="container">
          <h1>Register</h1>
          <p>Please fill in this form to create an account.</p>
          <hr />
          <form>


            <label for="email"><b>Email</b></label>
            <input type="text"
              value={currentEmail}
              placeholder="Enter Email"
              name="email" id="email" required
              onChange={e => setCurrentEmail(e.target.value)}
            />

            <label for="psw"><b>Password</b></label>
            <input type="password"
              value={currentPass}
              placeholder="Enter Password"
              name="psw"
              id="psw" required
              onChange={e => setCurrentPass(e.target.value)} />
          </form>
          <hr />
          <p>By creating an account you agree to our <a href="#">Terms & Privacy</a>.</p>

          <button type="submit" class="registerbtn">Update</button>
          <button type="submit" onClick={()=>setEdit(false)} class="registerbtn" style={{background:"gold"}} >Clear</button>
        </div>
          :
          <>
            <div class="container">
              <h1>Register</h1>
              <p>Please fill in this form to create an account.</p>
              <hr />
              <form>
                <label for="email"><b>Email</b></label>
                <input type="text"
                  value={email}
                  placeholder="Enter Email"
                  name="email" id="email" required
                  onChange={e => setEmail(e.target.value)}
                />

                <label for="psw"><b>Password</b></label>
                <input type="password"
                  value={password}
                  placeholder="Enter Password"
                  name="psw"
                  id="psw" required
                  onChange={e => setPassword(e.target.value)} />
              </form>
              <hr />
              <p>By creating an account you agree to our <a href="#">Terms & Privacy</a>.</p>

              <button type="submit" class="registerbtn" onClick={handleAdd}>Register</button>
            </div>

            <div class="container signin">
              <p>Already have an account? <a href="#">Sign in</a>.</p>
            </div>
            <table>
              <tr>
                <th>Id</th>
                <th>Email</th>
                <th>Password</th>
                <th>Option</th>
              </tr>
              {
                arrRegister.map((r) => {
                  return <tr key={r.id}>
                    <td>{r.id}</td>
                    <td>{r.email}</td>
                    <td>{r.password}</td>
                    <td><button className='btnDelete' onClick={() => deleteClick(r.id)}>Delete</button> <button className='btnEdit' onClick={()=>handleEditClick(r)}>Edit</button></td>
                  </tr>
                })
              }

            </table>

          </>
      }


    </div>
  );
}

export default App;
