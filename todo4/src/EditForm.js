import React from 'react'

const EditForm = ({setEditing,onEditFormSubmit,onEditInputChange,current}) => {
  return (
    <div>
        <form onSubmit={onEditFormSubmit}>
            <label>Enter Name</label>
            <input type="text"
            placeholder='Enter Name....' 
            setEditing={setEditing}
            value={current.text} //new value
            onChange={onEditInputChange} //change new
            />
            <button type='submit' style={{background:"rgb(184, 184, 19)"}} onClick={onEditFormSubmit} >Update</button>
            <button type='submit' style={{background:"rgb(219, 140, 30)"}} onClick={()=>setEditing(false)}>Clear</button>
          </form>
    </div>
  )
}

export default EditForm