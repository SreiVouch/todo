import './index.css'
import { BiEdit } from 'react-icons/bi'
import { AiOutlineDelete } from 'react-icons/ai'
import { useEffect, useState } from 'react';
import AddFormTodo from './AddFormTodo';
import EditForm from './EditForm';
function App() {
  const [todos,setTodos]=useState(()=>{
    const saveTodos =localStorage.getItem("todos");
    if(saveTodos){
      return JSON.parse(saveTodos)
    }else{
      return [];
    }
  });
  const [todo,setTodo]=useState("");

  const handleAddInputChange=e=>{
    setTodo(e.target.value)
  }
  const handleAddFormSubmit=e=>{
    e.preventDefault();
    if(todo!==""){
      setTodos([
        ...todos,{
          id:Math.floor(Math.random()*1000),
          text:todo.trim()

        }
      ])
     
    }
    setTodo('');
  }
  useEffect(()=>{
    localStorage.setItem("todos",JSON.stringify(todos))
  },[todos])
  // console.log(localStorage)
  const handleDeleteClick=id=>{
    //[...todos].filter it mean remove date in todos
    const removeArr=[...todos].filter(todo=>todo.id!==id);
     setTodos(removeArr)
     alert(id)
   
  }

  const [isEditing,setEditing]=useState(false)
  const [currentTodo,setCurrentTodo]=useState({});
 //3
  const handleUpdate=(id,updateTodo)=>{
    const updateItem =todos.map((todo)=>{
      return todo.id===id ? updateTodo:todo //Ex  old =Manin update=Man => setTodos(Man)  old ID =new ID
    })
    setEditing(false)
    setTodos(updateItem)
    
    // alert(id)991
    
  }
  //2
  const handleEditInputChange =e=>{
    setCurrentTodo({...currentTodo,text:e.target.value})
   
  }

  //4
  const handEditFormSubmit=e=>{
    e.preventDefault()
      handleUpdate(currentTodo.id,currentTodo) //newID
      // alert(currentTodo.id)991
  }
  //EditClick1
  const handleEditClick=todo=>{
    setEditing(true);
    setCurrentTodo({...todo})
   
  }
  return (
    <div className="App">
      <div className='form_add'>
        {
          isEditing?(<EditForm setEditing={isEditing} current={currentTodo} onEditFormSubmit={handEditFormSubmit} onEditInputChange={handleEditInputChange}/>):  <AddFormTodo todo={todo} onAddInputChange={handleAddInputChange} onAddFormSubmit={handleAddFormSubmit}/>
        }
      
      
        <table >
          {
            todos.map((todo)=>{
              return <tr  key={todo.id} >
              <td>{todo.text}</td>
              <td  className="fa">
                <BiEdit style={{ color: "blue",cursor:" pointer",fontSize:"24px" }} onClick={()=>handleEditClick(todo)}/>
                <AiOutlineDelete style={{ color: "red",cursor:" pointer",marginLeft:"20px",fontSize:"24px"  }} onClick={()=>handleDeleteClick(todo.id)}/></td>
            </tr>
            })
          }
          
          
        </table>
      </div>
    </div>
  );
}

export default App;
