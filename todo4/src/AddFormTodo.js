import React from 'react'

const AddFormTodo = ({todo,onAddInputChange,onAddFormSubmit}) => {
  return (
    <div>
        <form onSubmit={onAddFormSubmit}>
          <label>Enter Name</label>
          <input type="text"
          todo={todo}
          placeholder='Enter Name....' 
          onChange={onAddInputChange}
          />
          <button type='submit'>Add</button>
        </form>
    </div>
  )
}

export default AddFormTodo