
import React, { useEffect, useState } from 'react';
import AddTodoForm from './AddTodoForm';
import './App.css';
import EditForm from './EditForm';

import TodoItem from './TodoItem';

function App() {
  const [todoss, setTodoss] = useState(() => {
    const saveTodoss = localStorage.getItem("todoss");
    if (saveTodoss) {
      return JSON.parse(saveTodoss);
    } else {
      return [];
    }
  });
  const [todo, setTodo] = useState("");
  const [isEditing, setIsEditing] = useState(false);
  const [currentTodo,setCurretTodo]=useState({});
  const handleUpdateTodo=(id,updateTodo)=>{
    const updateItem =todoss.map((todo)=>{
      return todo.id ===id ? updateTodo : todo;
    })
    setIsEditing(false);//clik on Update and it false change to AddTodoForm
    setTodoss(updateItem); // 3.bos update new value
    //to know
    // alert(id); 803
  }
  //after update 
  const handleEditFormsubmit=e=>{
    e.preventDefault();
    handleUpdateTodo(currentTodo.id,currentTodo);
    // alert(currentTodo.id) 803
  }
  

  //1.Edit old input take name in culunm to update
  const handleEditClick =todo=>{
    setIsEditing(true);//clik on Edti and it true change to EditFom
    setCurretTodo({...todo}) //=>valu={currentTodo} Ex:...todo=Vouch,Tida in formupdate
  }
  const handleEditInputChange=e=>{
    //2.change for update
    setCurretTodo({...currentTodo,text:e.target.value})
  }

  const handInputChange = e => {
    setTodo(e.target.value);
    
  }
  const handFormSubmt = e => {

    e.preventDefault();

    if (todo !== "") {
      setTodoss([
        ...todoss, {
          id: Math.floor(Math.random() * 1000),
          text: todo.trim()
        }
      ])
    }
    setTodo("");
  }
  useEffect(() => {
    localStorage.setItem("todoss", JSON.stringify(todoss));
  }, [todoss]);
  const handDeleteClick = id => {
    const removeArr = [...todoss].filter(todo => todo.id !== id);
    setTodoss(removeArr);
  }
  return (
    <div className="   block  items-center justify-between  border-2 mt-11 border-indigo-500 ">
      {isEditing ?
        <EditForm setEditing={setIsEditing} currentTodo={currentTodo} onEditFormSubmit={handleEditFormsubmit} onEditInputChange={handleEditInputChange}/>
        : (
          <AddTodoForm todo={todo} onAddFormSubmit={handFormSubmt} onAddInputChange={handInputChange} />
        )
      }
      <div className=' w-1/2   m-auto'>
        <div className=' m-auto w-full'>
          <table className='  border-2  border-indigo-500 w-full  text-center'>
            {
              todoss.map((todo) => {
                return <> <TodoItem todo={todo} onDeletClick={handDeleteClick} onEditClick={handleEditClick}/>
                </>
              })
            }
          </table>
        </div>


      </div>
    </div>
  );
}

export default App;
