import React from 'react'

const EditForm = ({currentTodo,setEditing,onEditFormSubmit, onEditInputChange}) => {
  return (
    <div className=' w-1/2    m-auto mb-4'>
    <div className='  m-auto w-full'>
        <form className=' p-3 bg-white  border-2 border-gray-200' onSubmit={onEditFormSubmit}>
            <h1>Todos</h1>
            <label><b>Email</b></label>
            <input
                type="text"
                name="todo"
                placeholder='Enter...'
                value={currentTodo.text}
                onChange={onEditInputChange}
                className=' w-full p-4 mt-1 ml-0 mb-6 mr-0 border-none outline-none bg-gray-100'
               
            />
            <button type='submit' className=' bg-yellow-500 text-white py-4 px-5 border-none cursor-pointer w-full mb-3 opacity-80' onClick={onEditFormSubmit} >Update</button>
            <button type='submit' className=' bg-red-400 text-white py-4 px-5 border-none cursor-pointer w-full mb-3 opacity-80' onClick={()=>setEditing(false)}>Cancel</button>
        </form>
    </div>
</div>
  )
}

export default EditForm