import React from 'react'
import {FaEdit} from'react-icons/fa'
import {TiDelete} from 'react-icons/ti'
const TodoItem = ({ todo,onDeletClick,onEditClick}) => {
    return (
       
            
          <tr key={todo.id}><td>{todo.text}</td>
                  <td className=' flex '><FaEdit className=' ml-2 text-indigo-700 text-xl cursor-pointer' onClick={()=>onEditClick(todo)}/><TiDelete className=' text-xl  ml-4 text-red-700 cursor-pointer' onClick={()=>onDeletClick(todo.id)}/></td>
            </tr>
                       
      
    )
}

export default TodoItem