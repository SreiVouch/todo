import React from 'react'

const AddTodoForm = ({ todo, onAddFormSubmit, onAddInputChange }) => {
    return (
        <div>
            <div className=' w-1/2    m-auto mb-4'>
                <div className='  m-auto w-full'>
                    <form className=' p-3 bg-white  border-2 border-gray-200' onSubmit={onAddFormSubmit} >
                        <h1>Todos</h1>
                        <label><b>Email</b></label>
                        <input
                            type="text"
                            name="todo"
                            placeholder='Enter...'
                            className=' w-full p-4 mt-1 ml-0 mb-6 mr-0 border-none outline-none bg-gray-100'
                            todo={todo}
                            onChange={onAddInputChange}
                        />
                        <button type='submit' className=' bg-green-400 text-white py-4 px-5 border-none cursor-pointer w-full mb-3 opacity-80'>Add</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default AddTodoForm