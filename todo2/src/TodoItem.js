import React from 'react'
import {FaEdit} from'react-icons/fa'
import {TiDelete} from 'react-icons/ti'

const TodoItem = ({todo,onDeleteClick,onEditClick}) => {
  return (
    <li key={todo.id}>
      {todo.text}
      <button className='fa' style={{color:"blue"}} onClick={()=> onEditClick(todo)} >Edit<FaEdit/></button>
    <button className='fa' style={{color:"red"}} onClick={()=> onDeleteClick(todo.id)} >Delete<TiDelete/></button>
    </li>
 
  )
}

export default TodoItem