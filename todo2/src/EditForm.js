import React from 'react'

const EditForm = ({
    currentTodo,
    setEditing,
    onEditInputChange,
    onEditFormSubmit}) => {
  return (
    <div>
         <form className='form' onSubmit={onEditFormSubmit}>
            <input placeholder='Enter...'
                name='todo'
                type="text"
                value={currentTodo.text}
                onChange={onEditInputChange}
            />
            <button className='button' type='submit' onClick={onEditFormSubmit} >Update</button>
            <button className='button' type='submit' onClick={()=> setEditing(false)} >Cancel</button>
        </form>
    </div>
  )
}

export default EditForm