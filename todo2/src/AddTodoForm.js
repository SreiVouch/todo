import React from 'react'
import './App.css'

const AddTodoForm = ({todo,onAddFormSubmit,onAddInputChange}) => {
    return (
        <form className='form' onSubmit={onAddFormSubmit}>
            <input placeholder='Enter...'
                name='todo'
                type="text"
                value={todo}
                onChange={onAddInputChange}
            />
            <button className='button' type='submit'  >Add</button>
        </form>
    )
}

export default AddTodoForm