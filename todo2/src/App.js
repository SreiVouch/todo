
import React, { useEffect, useState } from 'react';
import AddTodoForm from './AddTodoForm';
import './App.css';
import EditForm from './EditForm';
import TodoItem from './TodoItem';

function App() {
  //ត្រូវការ​​Stateដើម្បីរក្សា​Track of Todo
  const [todos, setTodos] = useState(()=>{
    // ទទួល​the todos ពី​localstorage
    const saveTodos=localStorage.getItem("todos");
    //ប្រសិនបើtodoបានស្តរ
    if(saveTodos){
      //បញ្ចូន​the parsed JSON obj ទៅ​ JS​ obj
     return JSON.parse(saveTodos);

    }else{
      //បញ្ញូនទៅ array ដែលគ្មាន
     return [];
    }
   });
   //ត្រូវការ​ State ដើម្បីរក្សា​​ តម្លៃ
  const [todo, setTodo] = useState("");
  // boolean state ដើម្បីដឹងប្រសិនបើ​យើង​Edit (និងlet display)
  //different input ទៅលើcondition
  const [isEditing,setEIsEdition]=useState(false);
  //obj state to set so យើងដឹងដែលTodo item editiing​
  const [currentTodo,setCurrentTodo]=useState({});
   
  //ជាfunction ដើម្បីទទូលតម្លៃ​ពេលយើងបញ្ចូល​ ហ់ើយយើងset តម្លៃថ្មី
  const handAddInputChange=e=>{
    //set តម្លៃថ្មី​ទៅនិងអ្វីដែលវាកំពុងកើតមានក្នុងប្រអប់
      setTodo(e.target.value);
    }
  //func ដើម្បីទទួល​តម្លៃ​នៃការedit input ហើយ​​ ចាប់យក​state ថ្មី
  const handleEditInputChange=e=>{
    //set ចាប់យក​ តម្លៃstate ថ្មី​ ដែល​ដំណើរក្នុង​​edit input​​
    setCurrentTodo({...currentTodo , text : e.target.value});
    console.log(currentTodo);
  }
  //ជាfuncដើម្បីបង្កើត​ obj ថ្មីមួយ​​ទៅលើForm submit
  const handFormSubmt = e => {
    //prevent the browser default វាមានលក្ខណះ​ ជាការ​​ refresh page នៅលើ submit
    e.preventDefault();
    //មិន​អាចsubmitប្រសិន​កន្លែងInput ទទេ​(empty string)
    if (todo !== "") {
      //set the new todo state (the array)
      setTodos([
        //យក​តម្លៃនៅក្នុងstate
        ...todos , {
          // basic id of the obj
          id:Math.floor(Math.random()*1000), 
          //យក​​​​តម្លៃTEXT​​នៃ​TODO STATE AND
          //Trim space pi input​
          text:todo.trim()

        }
      ])
    }
    // clear out the input box{clearអក្សរចេញ}
    setTodo("");
  }

  const handEditFromSubmit=e=>{
    e.preventDefault();
 //call the handleUpdateTodo func passin the currentTodo.id and the current obj as agurment
    handUpdateTodo(currentTodo.id,currentTodo);
  }
  
  const handDeleteClick = id => {
    const removeArr=[...todos].filter(todo => todo.id !== id);
    setTodos(removeArr);
    console.log(removeArr)
} 

  // //ជា​func​ដែល​លុបTodo ពី​ array
  // const handDeleteClick=id=>{
  //   //remove item from todo array on a button click
  //   const removeItem = todos.filter((todo)=>{
  //     //retrun the rest of the todos that don't math the item we are delete
  //     return todo.id!== id;
  //   });
  //   //remove return a new array
  //   setTodos(removeItem);
  // }
  // func to edit a todo item
   const handUpdateTodo=(id,updateTodo)=>{
    //mappimg over todos arr  
    //if id's match , use the second paramater to pass in update
    //just use old todo
    const updateItem = todos.map((todo)=>{
      return todo.id ===id ?updateTodo :todo;
    })
    // set editing to false cos this func will be use a onSubmit funtion
    setEIsEdition(false);
    //update the todos state with the update todo
    setTodos(updateItem);
  }

   //ជាfunc​ ដែលhandl ពេល​​ Edit buttonClick
   const handleEditeClick=todo=>{
    //set editing is true
    setEIsEdition(true);
    //set the currentTodo to the todo item ដែល​​Clicked
    setCurrentTodo({...todo});
  }

  //useEffect ដើម្បីrun​​ the component mounts
  useEffect(()=>{
    //localstorage វា​sppតែ string like ​key and value
    //ហើយយើងមិនអាចផ្ទុក​arr​​និង​obj ដោយគ្មាន​ការ​ ​covert Obj ទេ 
   //​បញ្ចូលstringដំបូង​។JSON.stringify និង​ conver obj JSON string មួយ
    localStorage.setItem("todos",JSON.stringify(todos));
    //add todo ដូច​​​​ dependancy ​ព្រោះយើងចង់​Update
    //localstorage ផ្សេង​ៗ​​​ នៃtodo state changes
  },[todos]);
  // const handleAddFormSumit=e=>{
  //   e.preventDefault();
  //   if(todo!==""){
  //     setTodos([
  //       ...todos,{
  //         id: new Date(),
  //         text:todos.trim()
  //       }
  //     ]);
  //   }
  //   setTodo("");
  // }
  return (
    <>

      <div className='box'>
        {isEditing ?<EditForm
        currentTodo={currentTodo}
        setEditing={setEIsEdition}
         onEditInputChange={handleEditInputChange}
         onEditFormSubmit={handEditFromSubmit}
         
        //  onEditFormSubmit={}
         />
         :(<AddTodoForm 
          todo={todo} 
          onAddInputChange={handAddInputChange}
          onAddFormSubmit={handFormSubmt}
          />
        )}
        <ul className='ul'>
          {todos.map((todo)=>{
            return<>
            <TodoItem 
            todo={todo}
            onEditClick={handleEditeClick}
            onDeleteClick={handDeleteClick}/>
            
            </> 
          })}
        </ul>
      </div>

    </>
  );
}

export default App;
